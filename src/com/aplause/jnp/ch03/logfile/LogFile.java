package com.aplause.jnp.ch03.logfile;

import java.io.*;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by aplause on 2015-08-05.
 */
public class LogFile {
    private Writer out;

    public LogFile(File f) throws IOException {
        FileWriter fw = new FileWriter(f);
        this.out = new BufferedWriter(fw);
    }

    public synchronized void writeEntry(String message) throws IOException {
        Date d = Calendar.getInstance().getTime();
        out.write(d.toString());
        out.write('\t');
        out.write(message);
        out.write("\r\n");
    }

    public void close() throws IOException {
        out.flush();
        out.close();
    }
}

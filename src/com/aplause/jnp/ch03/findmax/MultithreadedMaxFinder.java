package com.aplause.jnp.ch03.findmax;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by aplause on 2015-08-02.
 */
public class MultithreadedMaxFinder {

    private static final int[] data = {3,1,345, 34, 93,23,23,40, 5043, 33, 320, 12, 32, 32,45,6543, 20349, 212567, 534, 4444454};

    public static int max(int[] data) throws ExecutionException, InterruptedException {
        if(data.length == 1) {
            return data[0];
        } else if(data.length == 0) {
            throw new IllegalArgumentException();
        }

        // split the job into 2 pieces
        FindMaxTask task1 = new FindMaxTask(data, 0, data.length/2);
        FindMaxTask task2 = new FindMaxTask(data, data.length/2, data.length);

        // spawn 2 thread
        ExecutorService service = Executors.newFixedThreadPool(2);
        Future<Integer> future1 = service.submit(task1);
        Future<Integer> future2 = service.submit(task2);

        return Math.max(future1.get(), future2.get());
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        System.out.println("Maximum is: "  + MultithreadedMaxFinder.max(data));

    }
}

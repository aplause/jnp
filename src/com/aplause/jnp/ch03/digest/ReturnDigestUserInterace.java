package com.aplause.jnp.ch03.digest;

import javax.xml.bind.DatatypeConverter;

/**
 * Created by aplause on 2015-08-02.
 */
public class ReturnDigestUserInterace {

    public static void main(String[] args) {

        ReturnDigest[] digests = new ReturnDigest[args.length];

        for (int i = 0; i < args.length; i++) {
            digests[i] = new ReturnDigest(args[i]);
            digests[i].start();
        }
        for (int i = 0; i < args.length; i++) {
            while(true) {
                byte[] digest = digests[i].getDigest();
                if(null != digest) {
                    StringBuilder result = new StringBuilder(args[i]);
                    result.append(": ");

                    result.append(DatatypeConverter.printHexBinary(digest));
                    System.out.println(result);
                    break;
                }
            }






        }
    }

}

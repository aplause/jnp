package com.aplause.jnp.ch03.digest;

import javax.xml.bind.DatatypeConverter;

/**
 * Created by aplause on 2015-08-02.
 */

/*This is the last example of Diggest calculator thread in this chapter

 */
public class InstanceCallbackDigestUserInterface {
    private String filename;
    private byte[] digest;

    public InstanceCallbackDigestUserInterface(String filename) {
        this.filename = filename;
    }

    public void calculateDigest() {
        InstanceCallbackDigest cb = new InstanceCallbackDigest(filename, this);
        Thread t = new Thread(cb);
        t.start();
    }
    public void receiveDigest(byte[] digest) {
        this.digest = digest;
        System.out.println(this);
    }

    @Override
    public String toString() {
        String result = filename + ": ";
        if(digest != null) {
            result += DatatypeConverter.printHexBinary(digest);
        } else {
            result += "digesg not available";
        }
        return result;
    }

    public static void main(String[] args) {
        for(String filename : args) {
            InstanceCallbackDigestUserInterface d = new InstanceCallbackDigestUserInterface(filename);
            d.calculateDigest();
        }
    }
}

package com.aplause.jnp.ch03.gzip;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by aplause on 2015-08-06.
 */
public class GZipALlFiles {

    public final static int THREAD_COUNT = 4;

    public static void main(String[] args) {
        ExecutorService pool = Executors.newFixedThreadPool(THREAD_COUNT);

        for(String filename : args) {
            File f = new File(filename);
            if(f.exists()) {
                if(f.isDirectory()) {
                    File[] files = f.listFiles();
                    for(int i = 0; i < files.length ; i++) {
                        if(!files[i].isDirectory()) {
                            Runnable task = new GzipRunnable(files[i]);
                            pool.submit(task);
                        }
                    }
                } else {
                    Runnable task = new GzipRunnable(f);
                    pool.submit(task);
                }
            }
        }
        pool.shutdown();;
    }
}

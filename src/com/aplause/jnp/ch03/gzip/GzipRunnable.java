package com.aplause.jnp.ch03.gzip;

import java.io.*;
import java.util.zip.GZIPOutputStream;

/**
 * Created by aplause on 2015-08-05.
 */
public class GzipRunnable implements Runnable{

    private final File input;

    public GzipRunnable(File input) {
        this.input = input;

    }
    @Override
    public void run() {
        //dont compress an already compressed file
        if(!input.getName().endsWith(".gz")) {
            File output = new File(input.getParent(), input.getName() + ".gz");
            // don't overriding exissting file
            if(!output.exists()) try {
                //with resourrces
                InputStream in = new BufferedInputStream(new FileInputStream(input));
                OutputStream out = new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(output)));{
                    int b;
                    while ((b = in.read()) != -1)
                        out.write(b);
                    out.flush();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

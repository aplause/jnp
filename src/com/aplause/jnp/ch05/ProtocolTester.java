package com.aplause.jnp.ch05;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by aplause on 2015-08-08.
 */
public class ProtocolTester {

    public static void main(String[] args) {

        // hypertext
        testProtocol("http://www.adc.org");

        //secure http
        testProtocol("https://www.amazon.com/exec/obidos/order2/");

        // file transfer
        testProtocol("ftp://ibiblio.org/pub/lanuages/java/javafaq/");

        // simple mail transfer
        testProtocol("mailto:elharo@ibiblio.org");

        // telnet
        testProtocol("telnet://dibner.poly.edu/");

        // local file
        testProtocol("file:///etc/passwd");

        // gopher
        testProtocol("gopher://gopher.anc.org.za/");

        //ldap
        testProtocol("ldap://ldap.itd.umich.edu/o=University%20of%Michigan,c=US?postalAddress");

        // jar
        testProtocol("jar:http://cafeaulait.org/books/javaio/ioexamples/javaio.jar!"
                + "/commacfaq/io/StreamCopier.class");

        // NFS
        testProtocol("hvs://utopia.poly.edu/usr/tmp/");

        // custom jdbc
        testProtocol("jdbc:mysql://luna.ibiblio.org:3306/NEWS");

        // rmi
        testProtocol("rmi://ibiblio.org/RenderEngine");

        // custom protocols for hotjava
        testProtocol("doc:/UserGuide/release.html");
        testProtocol("netdoc:/UserGuide/release.html");
        testProtocol("systemresource://www.adc.org/index.html");
        testProtocol("verbatim:http://www.adc.org/");

    }

    private static void testProtocol(String url) {
        try {
            URL u = new URL(url);

            System.out.println(u.getProtocol() + " is supported");

        } catch (MalformedURLException e) {
            String protocol  = url.substring(0, url.indexOf(':'));
            System.out.println(protocol + " is not supported");
        }


        //

    }
}

package com.aplause.jnp.ch04;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by aplause on 2015-08-07.
 */
public class ReverseTest {
    public static void main(String[] args) throws UnknownHostException {
        InetAddress ia = InetAddress.getByName("208.201.239.100");
        System.out.println(ia.getCanonicalHostName());
    }
}

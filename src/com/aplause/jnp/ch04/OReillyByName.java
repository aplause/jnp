package com.aplause.jnp.ch04;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by aplause on 2015-08-06.
 */
public class OReillyByName {

    public static void main(String[] args) {
        InetAddress address = null;
        try {
            address = InetAddress.getByName("www.oreilly.com");
            System.out.println(address);
        } catch (UnknownHostException e) {
            System.out.println("Could not find www.oreilly.com");
        }
    }
}

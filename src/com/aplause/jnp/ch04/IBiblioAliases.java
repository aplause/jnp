package com.aplause.jnp.ch04;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by aplause on 2015-08-07.
 */
public class IBiblioAliases {

    public static void main(String args[]) {
        try {
            InetAddress ibiblio = InetAddress.getByName("www.ibiblio.org");
            InetAddress helios = InetAddress.getByName("www.ibiblio.org");

            if(ibiblio.equals(helios)) {
                System.out.println("ibiblio is the same as helios");
            } else {
                System.out.println("ibiblio is not the same as helios");
            }
        } catch (UnknownHostException e) {
            System.out.println("Hos lookup failed.");
        }

    }
}
